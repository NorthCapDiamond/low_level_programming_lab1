#include <stdlib.h>
#include <stdio.h>
#include "graphql_ast.h"

ast_node* new_node() {
    ast_node* node = malloc(sizeof(ast_node));
    if (node) {
        *node = (ast_node) {0};
        return node;
    }
    return NULL;
}

ast_node* new_int_node(int32_t intVal) {
    ast_node* node = new_node();
    if (node) {
        node->type = INT_VAL_NODE;
        node->intVal = intVal;
    }
    return node;
}

ast_node* new_double_node(double doubleVal) {
    ast_node* node = new_node();
    if (node) {
        node->type = DOUBLE_VAL_NODE;
        node->doubleVal = doubleVal;
    }
    return node;
}

ast_node* new_bool_node(bool boolVal) {
    ast_node* node = new_node();
    if (node) {
        node->type = BOOL_VAL_NODE;
        node->boolVal = boolVal;
    }
    return node;
}

ast_node* new_str_node(char* strVal) {
    ast_node* node = new_node();
    if (node) {
        node->type = STR_VAL_NODE;
        node->strVal = strVal;
    }
    return node;
}

ast_node* new_key_node(char* key) {
    ast_node* node = new_node();
    if (node) {
        node->type = KEY_NODE;
        node->strVal = key;
    }
    return node;
}

ast_node* new_element_node(ast_node* keyNode, ast_node* valNode) {
    ast_node* node = new_node();
    if (node) {
        node->type = ELEMENT_NODE;
        node->left = keyNode;
        node->right = valNode;
    }
    return node;
}

ast_node* new_element_set_node(ast_node* elementNode) {
    ast_node* node = new_node();
    if (node) {
        node->type = ELEMENT_SET_NODE;
        node->left = elementNode;
    }
    return node;
}

ast_node* new_values_node(ast_node* elementSetNode) {
    ast_node* node = new_node();
    if (node) {
        node->type = VALUES_NODE;
        node->left = elementSetNode;
    }
    return node;
}

ast_node* new_operation_node(nodeType type, ast_node* left, ast_node* right) {
    ast_node* node = new_node();
    if (node) {
        node->type = type;
        node->left = left;
        node->right = right;
    }
    return node;
}

ast_node* new_filter_node(ast_node* operationNode) {
    ast_node* node = new_node();
    if (node) {
        node->type = FILTER_NODE;
        node->left = operationNode;
    }
    return node;
}

ast_node* new_object_node(char* schemaName, ast_node* valuesNode, ast_node* filterNode) {
    ast_node* node = new_node();
    if (node) {
        node->type = OBJECT_NODE;
        node->strVal = schemaName;
        node->left = valuesNode;
        node->right = filterNode;
    }
    return node;
}

ast_node* new_query_set_node(ast_node* queryNode) {
    ast_node* node = new_node();
    if (node) {
        node->type = QUERY_SET_NODE;
        node->left = queryNode;
    }
    return node;
}

ast_node* new_query_node(nodeType type, ast_node* objectNode, ast_node* querySetNode) {
    ast_node* node = new_node();
    if (node) {
        node->type = type;
        node->left = objectNode;
        node->right = querySetNode;
    }
    return node;
}

void add_next_element_to_set(ast_node* elementSetNode, ast_node* nextElementSetNode) {
    if (elementSetNode) {
        while (elementSetNode->right) {
            elementSetNode = elementSetNode->right;
        }
        elementSetNode->right = nextElementSetNode;
    }
}

void add_next_query_to_set(ast_node* querySetNode, ast_node* nextQuerySetNode) {
    if (querySetNode) {
        while (querySetNode->right) {
            querySetNode = querySetNode->right;
        }
        querySetNode->right = nextQuerySetNode;
    }
}

void destroy_node(ast_node* node) {
    if (node) {
        destroy_node(node->left);
        destroy_node(node->right);
        if ((node->type == STR_VAL_NODE || node->type == KEY_NODE || node->type == OBJECT_NODE) && node->strVal) {
            free(node->strVal);
        }
        free(node);
    }
}

void print_node(ast_node* node, int32_t nestingLevel) {
    if (node) {
        switch (node->type) {
            case QUERY_SELECT_NODE:
                printf("QueryType: Select\n");
                printf("QuerySet: ");
                print_node(node->right, nestingLevel + 2);
                break;
            case QUERY_INSERT_NODE:
                printf("QueryType: Insert\n");
                printf("QuerySet: ");
                print_node(node->right, nestingLevel + 2);
                break;
            case QUERY_UPDATE_NODE:
                printf("QueryType: Update\n");
                printf("QuerySet: ");
                print_node(node->right, nestingLevel + 2);
                break;
            case QUERY_DELETE_NODE:
                printf("QueryType: Delete\n");
                printf("QuerySet: ");
                print_node(node->right, nestingLevel + 2);
                break;
            case NESTED_QUERY_NODE:
                printf("%*sObject:\n", nestingLevel, "");
                print_node(node->left, nestingLevel + 2);
                printf("%*sQuerySet: ", nestingLevel, "");
                print_node(node->right, nestingLevel + 2);
                break;
            case QUERY_SET_NODE:
                printf("\n");
                while (node) {
                    printf("%*sQuery:\n", nestingLevel, "");
                    print_node(node->left, nestingLevel + 2);
                    node = node->right;
                }
                break;
            case OBJECT_NODE:
                printf("%*sSchemaName: %s\n", nestingLevel, "", node->strVal);
                printf("%*sNewValues: ", nestingLevel, "");
                print_node(node->left, nestingLevel + 2);
                printf("%*sFilter: ", nestingLevel, "");
                print_node(node->right, nestingLevel + 2);
                break;
            case VALUES_NODE:
                printf("\n%*sElementSet:\n", nestingLevel, "");
                print_node(node->left, nestingLevel + 2);
                break;
            case ELEMENT_SET_NODE:
                while (node) {
                    printf("%*sElement:\n", nestingLevel, "");
                    print_node(node->left, nestingLevel + 2);
                    node = node->right;
                }
                break;
            case ELEMENT_NODE:
                printf("%*sKey: %s\n", nestingLevel, "", node->left->strVal);
                printf("%*sValue:\n", nestingLevel, "");
                print_node(node->right, nestingLevel + 2);
                break;
            case INT_VAL_NODE:
                printf("%*sValueType: Integer\n", nestingLevel, "");
                printf("%*sData: %llu\n", nestingLevel, "", node->intVal);
                break;
            case DOUBLE_VAL_NODE:
                printf("%*sValueType: Double\n", nestingLevel, "");
                printf("%*sData: %f\n", nestingLevel, "", node->doubleVal);
                break;
            case BOOL_VAL_NODE:
                printf("%*sValueType: Boolean\n", nestingLevel, "");
                printf("%*sData: %s\n", nestingLevel, "", node->boolVal ? "true" : "false");
                break;
            case STR_VAL_NODE:
                printf("%*sValueType: String\n", nestingLevel, "");
                printf("%*sData: %s\n", nestingLevel, "", node->strVal);
                break;
            case KEY_NODE:
                printf("%s\n", node->strVal);
                break;
            case FILTER_NODE:
                printf("\n%*sOperation:\n", nestingLevel, "");
                print_node(node->left, nestingLevel + 2);
                break;
            case OP_EQ_NODE:
                printf("%*sOperationType: Equal\n", nestingLevel, "");
                if (node->right->type != KEY_NODE) {
                    printf("%*sKey: ", nestingLevel, "");
                    print_node(node->left, nestingLevel);
                    printf("%*sValue:\n", nestingLevel, "");
                    print_node(node->right, nestingLevel + 2);
                } else {
                    printf("%*sKey1: ", nestingLevel, "");
                    print_node(node->left, nestingLevel);
                    printf("%*sKey2: ", nestingLevel, "");
                    print_node(node->right, nestingLevel);
                }
                break;
            case OP_NEQ_NODE:
                printf("%*sOperationType: NotEqual\n", nestingLevel, "");
                if (node->right->type != KEY_NODE) {
                    printf("%*sKey: ", nestingLevel, "");
                    print_node(node->left, nestingLevel);
                    printf("%*sValue:\n", nestingLevel, "");
                    print_node(node->right, nestingLevel + 2);
                } else {
                    printf("%*sKey1: ", nestingLevel, "");
                    print_node(node->left, nestingLevel);
                    printf("%*sKey2: ", nestingLevel, "");
                    print_node(node->right, nestingLevel);
                }
                break;
            case OP_GT_NODE:
                printf("%*sOperationType: Greater\n", nestingLevel, "");
                if (node->right->type != KEY_NODE) {
                    printf("%*sKey: ", nestingLevel, "");
                    print_node(node->left, nestingLevel);
                    printf("%*sValue:\n", nestingLevel, "");
                    print_node(node->right, nestingLevel + 2);
                } else {
                    printf("%*sKey1: ", nestingLevel, "");
                    print_node(node->left, nestingLevel);
                    printf("%*sKey2: ", nestingLevel, "");
                    print_node(node->right, nestingLevel);
                }
                break;
            case OP_GTE_NODE:
                printf("%*sOperationType: GreaterOrEqual\n", nestingLevel, "");
                if (node->right->type != KEY_NODE) {
                    printf("%*sKey: ", nestingLevel, "");
                    print_node(node->left, nestingLevel);
                    printf("%*sValue:\n", nestingLevel, "");
                    print_node(node->right, nestingLevel + 2);
                } else {
                    printf("%*sKey1: ", nestingLevel, "");
                    print_node(node->left, nestingLevel);
                    printf("%*sKey2: ", nestingLevel, "");
                    print_node(node->right, nestingLevel);
                }
                break;
            case OP_LE_NODE:
                printf("%*sOperationType: Less\n", nestingLevel, "");
                if (node->right->type != KEY_NODE) {
                    printf("%*sKey: ", nestingLevel, "");
                    print_node(node->left, nestingLevel);
                    printf("%*sValue:\n", nestingLevel, "");
                    print_node(node->right, nestingLevel + 2);
                } else {
                    printf("%*sKey1: ", nestingLevel, "");
                    print_node(node->left, nestingLevel);
                    printf("%*sKey2: ", nestingLevel, "");
                    print_node(node->right, nestingLevel);
                }
                break;
            case OP_LEE_NODE:
                printf("%*sOperationType: LessOrEqual\n", nestingLevel, "");
                if (node->right->type != KEY_NODE) {
                    printf("%*sKey: ", nestingLevel, "");
                    print_node(node->left, nestingLevel);
                    printf("%*sValue:\n", nestingLevel, "");
                    print_node(node->right, nestingLevel + 2);
                } else {
                    printf("%*sKey1: ", nestingLevel, "");
                    print_node(node->left, nestingLevel);
                    printf("%*sKey2: ", nestingLevel, "");
                    print_node(node->right, nestingLevel);
                }
                break;
            case OP_LIKE_NODE:
                printf("%*sOperationType: Like\n", nestingLevel, "");
                if (node->right->type != KEY_NODE) {
                    printf("%*sKey: ", nestingLevel, "");
                    print_node(node->left, nestingLevel);
                    printf("%*sValue:\n", nestingLevel, "");
                    print_node(node->right, nestingLevel + 2);
                } else {
                    printf("%*sKey1: ", nestingLevel, "");
                    print_node(node->left, nestingLevel);
                    printf("%*sKey2: ", nestingLevel, "");
                    print_node(node->right, nestingLevel);
                }
                break;
            case OP_AND_NODE:
                printf("%*sOperationType: And\n", nestingLevel, "");
                printf("%*sOperation1:\n", nestingLevel, "");
                print_node(node->left, nestingLevel + 2);
                printf("%*sOperation2:\n", nestingLevel, "");
                print_node(node->right, nestingLevel + 2);
                break;
            case OP_OR_NODE:
                printf("%*sOperationType: Or\n", nestingLevel, "");
                printf("%*sOperation1:\n", nestingLevel, "");
                print_node(node->left, nestingLevel + 2);
                printf("%*sOperation2:\n", nestingLevel, "");
                print_node(node->right, nestingLevel + 2);
                break;
            case OP_NOT_NODE:
                printf("%*sOperationType: Not\n", nestingLevel, "");
                printf("%*sOperation:\n", nestingLevel, "");
                print_node(node->left, nestingLevel + 2);
                break;
        }
    } else {
        printf("<undefined>\n");
    }
}