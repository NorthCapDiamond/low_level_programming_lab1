/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "parser.y"

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include "graphql_ast.h"

#define MAX_NAME_LENGTH 12

extern int yylex();
extern int yylineno;

void yyerror(const char *msg) {
    fprintf(stderr, "Error on line %d: %s\n", yylineno, msg);
}

#line 88 "parser.tab.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "parser.tab.h"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_L_PARENTHESIS = 3,              /* L_PARENTHESIS  */
  YYSYMBOL_R_PARENTHESIS = 4,              /* R_PARENTHESIS  */
  YYSYMBOL_L_BRACE = 5,                    /* L_BRACE  */
  YYSYMBOL_R_BRACE = 6,                    /* R_BRACE  */
  YYSYMBOL_L_BRACKET = 7,                  /* L_BRACKET  */
  YYSYMBOL_R_BRACKET = 8,                  /* R_BRACKET  */
  YYSYMBOL_COMMA = 9,                      /* COMMA  */
  YYSYMBOL_COLON = 10,                     /* COLON  */
  YYSYMBOL_SELECT = 11,                    /* SELECT  */
  YYSYMBOL_INSERT = 12,                    /* INSERT  */
  YYSYMBOL_UPDATE = 13,                    /* UPDATE  */
  YYSYMBOL_DELETE = 14,                    /* DELETE  */
  YYSYMBOL_VALUES = 15,                    /* VALUES  */
  YYSYMBOL_FILTER = 16,                    /* FILTER  */
  YYSYMBOL_COMPARE_OP = 17,                /* COMPARE_OP  */
  YYSYMBOL_LIKE_OP = 18,                   /* LIKE_OP  */
  YYSYMBOL_LOGICAL_BOP = 19,               /* LOGICAL_BOP  */
  YYSYMBOL_LOGICAL_UOP = 20,               /* LOGICAL_UOP  */
  YYSYMBOL_BOOL = 21,                      /* BOOL  */
  YYSYMBOL_INT = 22,                       /* INT  */
  YYSYMBOL_DOUBLE = 23,                    /* DOUBLE  */
  YYSYMBOL_STRING = 24,                    /* STRING  */
  YYSYMBOL_NAME = 25,                      /* NAME  */
  YYSYMBOL_YYACCEPT = 26,                  /* $accept  */
  YYSYMBOL_init = 27,                      /* init  */
  YYSYMBOL_query = 28,                     /* query  */
  YYSYMBOL_select = 29,                    /* select  */
  YYSYMBOL_insert = 30,                    /* insert  */
  YYSYMBOL_update = 31,                    /* update  */
  YYSYMBOL_delete = 32,                    /* delete  */
  YYSYMBOL_select_next = 33,               /* select_next  */
  YYSYMBOL_insert_or_select_next = 34,     /* insert_or_select_next  */
  YYSYMBOL_insert_next = 35,               /* insert_next  */
  YYSYMBOL_update_next = 36,               /* update_next  */
  YYSYMBOL_select_object = 37,             /* select_object  */
  YYSYMBOL_mutate_object = 38,             /* mutate_object  */
  YYSYMBOL_schema_name = 39,               /* schema_name  */
  YYSYMBOL_values = 40,                    /* values  */
  YYSYMBOL_element = 41,                   /* element  */
  YYSYMBOL_key = 42,                       /* key  */
  YYSYMBOL_value = 43,                     /* value  */
  YYSYMBOL_filter = 44,                    /* filter  */
  YYSYMBOL_operation = 45,                 /* operation  */
  YYSYMBOL_compare_op = 46,                /* compare_op  */
  YYSYMBOL_like_op = 47,                   /* like_op  */
  YYSYMBOL_logical_op = 48                 /* logical_op  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  15
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   118

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  26
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  23
/* YYNRULES -- Number of rules.  */
#define YYNRULES  46
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  116

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   280


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int8 yyrline[] =
{
       0,    76,    76,    78,    78,    78,    78,    80,    82,    84,
      86,    88,    88,    88,    90,    90,    90,    92,    92,    92,
      94,    94,    94,    94,    96,    96,    98,    98,   100,   102,
     104,   104,   106,   108,   108,   108,   108,   110,   112,   112,
     112,   114,   114,   116,   116,   118,   118
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "L_PARENTHESIS",
  "R_PARENTHESIS", "L_BRACE", "R_BRACE", "L_BRACKET", "R_BRACKET", "COMMA",
  "COLON", "SELECT", "INSERT", "UPDATE", "DELETE", "VALUES", "FILTER",
  "COMPARE_OP", "LIKE_OP", "LOGICAL_BOP", "LOGICAL_UOP", "BOOL", "INT",
  "DOUBLE", "STRING", "NAME", "$accept", "init", "query", "select",
  "insert", "update", "delete", "select_next", "insert_or_select_next",
  "insert_next", "update_next", "select_object", "mutate_object",
  "schema_name", "values", "element", "key", "value", "filter",
  "operation", "compare_op", "like_op", "logical_op", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-79)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int8 yypact[] =
{
      -1,     3,    35,    46,    65,    74,   -79,   -79,   -79,   -79,
     -79,    55,    55,    55,    55,   -79,   -79,    17,    76,    79,
      50,   -79,    80,    81,    84,    51,    83,    85,    52,   -79,
      55,    55,    73,   -79,    55,    55,    55,    55,    61,   -79,
      55,    55,    55,   -79,   -79,    56,    82,    87,   -79,   -79,
      90,    57,    58,    86,     5,   -79,    62,    63,   -79,    30,
     -79,    88,   -79,   -79,    91,   -79,    73,   -79,   -79,    92,
      94,    96,    97,   -79,   -79,   -79,   -79,    89,    98,    93,
      93,    30,    30,    93,    70,   -79,   -79,    95,    99,   100,
     101,   102,   -79,    89,    21,    59,    30,   -79,    31,   -79,
     -79,   -79,   -79,   -79,   103,   106,   107,   109,   110,   111,
     -79,   -79,   -79,   -79,   -79,   -79
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int8 yydefact[] =
{
       0,     0,     0,     0,     0,     0,     2,     3,     4,     5,
       6,     0,     0,     0,     0,     1,    28,     0,    11,    24,
       0,    14,     0,    17,    24,     0,     0,    20,     0,     7,
       0,     0,     0,     8,     0,     0,     0,     0,     0,     9,
       0,     0,     0,    10,    13,     0,     0,     0,    16,    19,
       0,     0,     0,     0,     0,    23,     0,     0,    12,     0,
      25,     0,    15,    18,     0,    26,     0,    22,    21,     0,
       0,     0,     0,    37,    38,    39,    40,     0,     0,     0,
       0,     0,     0,     0,     0,    27,    32,     0,     0,     0,
       0,     0,    29,     0,     0,     0,     0,    45,     0,    31,
      33,    34,    35,    36,     0,     0,     0,     0,     0,     0,
      42,    41,    43,    44,    46,    30
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -79,   -79,   -79,   -79,   -79,   -79,   -79,   -11,    39,   -10,
      -4,   -12,    -9,     4,   -79,     8,   -78,    18,    40,   -75,
     -79,   -79,   -79
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
       0,     5,     6,     7,     8,     9,    10,    17,    20,    21,
      25,    18,    23,    24,    54,    84,    87,   105,    47,    73,
      74,    75,    76
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int8 yytable[] =
{
      22,    26,    88,    28,    27,    91,    89,    90,    11,    65,
       1,     2,     3,     4,    66,    19,   104,   107,    19,    44,
      45,   108,    22,    29,    22,    49,    30,    52,    26,    26,
      26,    27,    27,    27,    19,    19,    55,    56,    57,    50,
      12,    50,   100,   101,   102,   103,    86,    69,    70,    71,
      72,    13,   100,   101,   102,   103,    33,    39,    43,    34,
      40,    30,    58,    62,    63,    30,    34,    35,    67,    68,
      14,    40,    40,    48,    15,    51,    53,    46,    92,    93,
      16,    31,    32,   106,    86,    36,    37,    38,    41,    46,
      42,    60,    59,    61,    83,    79,    64,    80,    77,    81,
      82,    99,    85,    53,    94,    97,    78,   110,    95,    96,
     111,   112,    98,   113,   114,     0,   109,   115,    86
};

static const yytype_int8 yycheck[] =
{
      12,    13,    80,    14,    13,    83,    81,    82,     5,     4,
      11,    12,    13,    14,     9,    11,    94,    95,    14,    30,
      31,    96,    34,     6,    36,    35,     9,    37,    40,    41,
      42,    40,    41,    42,    30,    31,    40,    41,    42,    35,
       5,    37,    21,    22,    23,    24,    25,    17,    18,    19,
      20,     5,    21,    22,    23,    24,     6,     6,     6,     9,
       9,     9,     6,     6,     6,     9,     9,     9,     6,     6,
       5,     9,     9,    34,     0,    36,    15,    16,     8,     9,
      25,     5,     3,    24,    25,     5,     5,     3,     5,    16,
       5,     4,    10,     3,     5,     3,    10,     3,     7,     3,
       3,    93,     4,    15,     9,     4,    66,     4,     9,     9,
       4,     4,    10,     4,     4,    -1,    98,     6,    25
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,    11,    12,    13,    14,    27,    28,    29,    30,    31,
      32,     5,     5,     5,     5,     0,    25,    33,    37,    39,
      34,    35,    37,    38,    39,    36,    37,    38,    33,     6,
       9,     5,     3,     6,     9,     9,     5,     5,     3,     6,
       9,     5,     5,     6,    33,    33,    16,    44,    34,    35,
      39,    34,    35,    15,    40,    36,    36,    36,     6,    10,
       4,     3,     6,     6,    10,     4,     9,     6,     6,    17,
      18,    19,    20,    45,    46,    47,    48,     7,    44,     3,
       3,     3,     3,     5,    41,     4,    25,    42,    42,    45,
      45,    42,     8,     9,     9,     9,     9,     4,    10,    41,
      21,    22,    23,    24,    42,    43,    24,    42,    45,    43,
       4,     4,     4,     4,     4,     6
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr1[] =
{
       0,    26,    27,    28,    28,    28,    28,    29,    30,    31,
      32,    33,    33,    33,    34,    34,    34,    35,    35,    35,
      36,    36,    36,    36,    37,    37,    38,    38,    39,    40,
      41,    41,    42,    43,    43,    43,    43,    44,    45,    45,
      45,    46,    46,    47,    47,    48,    48
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     1,     1,     1,     1,     1,     4,     4,     4,
       4,     1,     4,     3,     1,     4,     3,     1,     4,     3,
       1,     4,     4,     3,     1,     4,     4,     6,     1,     5,
       5,     3,     1,     1,     1,     1,     1,     3,     1,     1,
       1,     6,     6,     6,     6,     4,     6
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)




# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)]);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep)
{
  YY_USE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;




/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* init: query  */
#line 76 "parser.y"
            { print_node((yyvsp[0].node), 0); destroy_node((yyvsp[0].node)); YYACCEPT; }
#line 1190 "parser.tab.c"
    break;

  case 3: /* query: select  */
#line 78 "parser.y"
              { (yyval.node) = (yyvsp[0].node); }
#line 1196 "parser.tab.c"
    break;

  case 4: /* query: insert  */
#line 78 "parser.y"
                                    { (yyval.node) = (yyvsp[0].node); }
#line 1202 "parser.tab.c"
    break;

  case 5: /* query: update  */
#line 78 "parser.y"
                                                          { (yyval.node) = (yyvsp[0].node); }
#line 1208 "parser.tab.c"
    break;

  case 6: /* query: delete  */
#line 78 "parser.y"
                                                                                { (yyval.node) = (yyvsp[0].node); }
#line 1214 "parser.tab.c"
    break;

  case 7: /* select: SELECT L_BRACE select_next R_BRACE  */
#line 80 "parser.y"
                                           { (yyval.node) = new_query_node(QUERY_SELECT_NODE, NULL, (yyvsp[-1].node)); }
#line 1220 "parser.tab.c"
    break;

  case 8: /* insert: INSERT L_BRACE insert_or_select_next R_BRACE  */
#line 82 "parser.y"
                                                     { (yyval.node) = new_query_node(QUERY_INSERT_NODE, NULL, (yyvsp[-1].node)); }
#line 1226 "parser.tab.c"
    break;

  case 9: /* update: UPDATE L_BRACE update_next R_BRACE  */
#line 84 "parser.y"
                                           { (yyval.node) = new_query_node(QUERY_UPDATE_NODE, NULL, (yyvsp[-1].node)); }
#line 1232 "parser.tab.c"
    break;

  case 10: /* delete: DELETE L_BRACE select_next R_BRACE  */
#line 86 "parser.y"
                                           { (yyval.node) = new_query_node(QUERY_DELETE_NODE, NULL, (yyvsp[-1].node)); }
#line 1238 "parser.tab.c"
    break;

  case 11: /* select_next: select_object  */
#line 88 "parser.y"
                           { (yyval.node) = new_query_set_node(new_query_node(NESTED_QUERY_NODE, (yyvsp[0].node), NULL)); }
#line 1244 "parser.tab.c"
    break;

  case 12: /* select_next: select_object L_BRACE select_next R_BRACE  */
#line 88 "parser.y"
                                                                                                                                                 { (yyval.node) = new_query_set_node(new_query_node(NESTED_QUERY_NODE, (yyvsp[-3].node), (yyvsp[-1].node))); }
#line 1250 "parser.tab.c"
    break;

  case 13: /* select_next: select_next COMMA select_next  */
#line 88 "parser.y"
                                                                                                                                                                                                                                                         { add_next_query_to_set((yyvsp[-2].node), (yyvsp[0].node)); (yyval.node) = (yyvsp[-2].node); }
#line 1256 "parser.tab.c"
    break;

  case 14: /* insert_or_select_next: insert_next  */
#line 90 "parser.y"
                                               { (yyval.node) = (yyvsp[0].node); }
#line 1262 "parser.tab.c"
    break;

  case 15: /* insert_or_select_next: select_object L_BRACE insert_or_select_next R_BRACE  */
#line 90 "parser.y"
                                                                                                                  { (yyval.node) = new_query_set_node(new_query_node(NESTED_QUERY_NODE, (yyvsp[-3].node), (yyvsp[-1].node))); }
#line 1268 "parser.tab.c"
    break;

  case 16: /* insert_or_select_next: insert_or_select_next COMMA insert_or_select_next  */
#line 90 "parser.y"
                                                                                                                                                                                                                                              { add_next_query_to_set((yyvsp[-2].node), (yyvsp[0].node)); (yyval.node) = (yyvsp[-2].node); }
#line 1274 "parser.tab.c"
    break;

  case 17: /* insert_next: mutate_object  */
#line 92 "parser.y"
                           { (yyval.node) = new_query_set_node(new_query_node(NESTED_QUERY_NODE, (yyvsp[0].node), NULL)); }
#line 1280 "parser.tab.c"
    break;

  case 18: /* insert_next: mutate_object L_BRACE insert_next R_BRACE  */
#line 92 "parser.y"
                                                                                                                                                 { (yyval.node) = new_query_set_node(new_query_node(NESTED_QUERY_NODE, (yyvsp[-3].node), (yyvsp[-1].node))); }
#line 1286 "parser.tab.c"
    break;

  case 19: /* insert_next: insert_next COMMA insert_next  */
#line 92 "parser.y"
                                                                                                                                                                                                                                                         { add_next_query_to_set((yyvsp[-2].node), (yyvsp[0].node)); (yyval.node) = (yyvsp[-2].node); }
#line 1292 "parser.tab.c"
    break;

  case 20: /* update_next: mutate_object  */
#line 94 "parser.y"
                           { (yyval.node) = new_query_set_node(new_query_node(NESTED_QUERY_NODE, (yyvsp[0].node), NULL)); }
#line 1298 "parser.tab.c"
    break;

  case 21: /* update_next: mutate_object L_BRACE update_next R_BRACE  */
#line 94 "parser.y"
                                                                                                                                                 { (yyval.node) = new_query_set_node(new_query_node(NESTED_QUERY_NODE, (yyvsp[-3].node), (yyvsp[-1].node))); }
#line 1304 "parser.tab.c"
    break;

  case 22: /* update_next: select_object L_BRACE update_next R_BRACE  */
#line 94 "parser.y"
                                                                                                                                                                                                                                                                     { (yyval.node) = new_query_set_node(new_query_node(NESTED_QUERY_NODE, (yyvsp[-3].node), (yyvsp[-1].node))); }
#line 1310 "parser.tab.c"
    break;

  case 23: /* update_next: update_next COMMA update_next  */
#line 94 "parser.y"
                                                                                                                                                                                                                                                                                                                                                                             { add_next_query_to_set((yyvsp[-2].node), (yyvsp[0].node)); (yyval.node) = (yyvsp[-2].node); }
#line 1316 "parser.tab.c"
    break;

  case 24: /* select_object: schema_name  */
#line 96 "parser.y"
                           { (yyval.node) = new_object_node((yyvsp[0].my_str), NULL, NULL); }
#line 1322 "parser.tab.c"
    break;

  case 25: /* select_object: schema_name L_PARENTHESIS filter R_PARENTHESIS  */
#line 96 "parser.y"
                                                                                                                      { (yyval.node) = new_object_node((yyvsp[-3].my_str), NULL, (yyvsp[-1].node)); }
#line 1328 "parser.tab.c"
    break;

  case 26: /* mutate_object: schema_name L_PARENTHESIS values R_PARENTHESIS  */
#line 98 "parser.y"
                                                              { (yyval.node) = new_object_node((yyvsp[-3].my_str), (yyvsp[-1].node), NULL); }
#line 1334 "parser.tab.c"
    break;

  case 27: /* mutate_object: schema_name L_PARENTHESIS values COMMA filter R_PARENTHESIS  */
#line 98 "parser.y"
                                                                                                                                                                    { (yyval.node) = new_object_node((yyvsp[-5].my_str), (yyvsp[-3].node), (yyvsp[-1].node)); }
#line 1340 "parser.tab.c"
    break;

  case 28: /* schema_name: NAME  */
#line 100 "parser.y"
                  { if(strlen((yyvsp[0].my_str)) > MAX_NAME_LENGTH) { yyerror("name of schema is too long"); YYABORT; } (yyval.my_str) = (yyvsp[0].my_str); }
#line 1346 "parser.tab.c"
    break;

  case 29: /* values: VALUES COLON L_BRACKET element R_BRACKET  */
#line 102 "parser.y"
                                                 { (yyval.node) = new_values_node((yyvsp[-1].node)); }
#line 1352 "parser.tab.c"
    break;

  case 30: /* element: L_BRACE key COLON value R_BRACE  */
#line 104 "parser.y"
                                         { (yyval.node) = new_element_set_node(new_element_node((yyvsp[-3].node), (yyvsp[-1].node))); }
#line 1358 "parser.tab.c"
    break;

  case 31: /* element: element COMMA element  */
#line 104 "parser.y"
                                                                                                                          { add_next_element_to_set((yyvsp[-2].node), (yyvsp[0].node)); (yyval.node) = (yyvsp[-2].node); }
#line 1364 "parser.tab.c"
    break;

  case 32: /* key: NAME  */
#line 106 "parser.y"
          { if(strlen((yyvsp[0].my_str)) > MAX_NAME_LENGTH) { yyerror("key is too long"); YYABORT; } (yyval.node) = new_key_node((yyvsp[0].my_str)); }
#line 1370 "parser.tab.c"
    break;

  case 33: /* value: BOOL  */
#line 108 "parser.y"
            { (yyval.node) = new_bool_node((yyvsp[0].my_bool)); }
#line 1376 "parser.tab.c"
    break;

  case 34: /* value: INT  */
#line 108 "parser.y"
                                              { (yyval.node) = new_int_node((yyvsp[0].my_int)); }
#line 1382 "parser.tab.c"
    break;

  case 35: /* value: DOUBLE  */
#line 108 "parser.y"
                                                                                  { (yyval.node) = new_double_node((yyvsp[0].my_double)); }
#line 1388 "parser.tab.c"
    break;

  case 36: /* value: STRING  */
#line 108 "parser.y"
                                                                                                                         { (yyval.node) = new_str_node((yyvsp[0].my_str)); }
#line 1394 "parser.tab.c"
    break;

  case 37: /* filter: FILTER COLON operation  */
#line 110 "parser.y"
                               { (yyval.node) = new_filter_node((yyvsp[0].node)); }
#line 1400 "parser.tab.c"
    break;

  case 38: /* operation: compare_op  */
#line 112 "parser.y"
                      { (yyval.node) = (yyvsp[0].node); }
#line 1406 "parser.tab.c"
    break;

  case 39: /* operation: like_op  */
#line 112 "parser.y"
                                             { (yyval.node) = (yyvsp[0].node); }
#line 1412 "parser.tab.c"
    break;

  case 40: /* operation: logical_op  */
#line 112 "parser.y"
                                                                       { (yyval.node) = (yyvsp[0].node); }
#line 1418 "parser.tab.c"
    break;

  case 41: /* compare_op: COMPARE_OP L_PARENTHESIS key COMMA value R_PARENTHESIS  */
#line 114 "parser.y"
                                                                   { (yyval.node) = new_operation_node((yyvsp[-5].op_type), (yyvsp[-3].node), (yyvsp[-1].node)); }
#line 1424 "parser.tab.c"
    break;

  case 42: /* compare_op: COMPARE_OP L_PARENTHESIS key COMMA key R_PARENTHESIS  */
#line 114 "parser.y"
                                                                                                                                                                   { (yyval.node) = new_operation_node((yyvsp[-5].op_type), (yyvsp[-3].node), (yyvsp[-1].node)); }
#line 1430 "parser.tab.c"
    break;

  case 43: /* like_op: LIKE_OP L_PARENTHESIS key COMMA STRING R_PARENTHESIS  */
#line 116 "parser.y"
                                                              { (yyval.node) = new_operation_node((yyvsp[-5].op_type), (yyvsp[-3].node), new_str_node((yyvsp[-1].my_str))); }
#line 1436 "parser.tab.c"
    break;

  case 44: /* like_op: LIKE_OP L_PARENTHESIS key COMMA key R_PARENTHESIS  */
#line 116 "parser.y"
                                                                                                                                                                         { (yyval.node) = new_operation_node((yyvsp[-5].op_type), (yyvsp[-3].node), (yyvsp[-1].node)); }
#line 1442 "parser.tab.c"
    break;

  case 45: /* logical_op: LOGICAL_UOP L_PARENTHESIS operation R_PARENTHESIS  */
#line 118 "parser.y"
                                                              { (yyval.node) = new_operation_node((yyvsp[-3].op_type), (yyvsp[-1].node), NULL); }
#line 1448 "parser.tab.c"
    break;

  case 46: /* logical_op: LOGICAL_BOP L_PARENTHESIS operation COMMA operation R_PARENTHESIS  */
#line 118 "parser.y"
                                                                                                                                                                             { (yyval.node) = new_operation_node((yyvsp[-5].op_type), (yyvsp[-3].node), (yyvsp[-1].node)); }
#line 1454 "parser.tab.c"
    break;


#line 1458 "parser.tab.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (YY_("syntax error"));
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 120 "parser.y"

