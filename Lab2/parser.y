%{
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include "graphql_ast.h"

#define MAX_NAME_LENGTH 12

extern int yylex();
extern int yylineno;

void yyerror(const char *msg) {
    fprintf(stderr, "Error on line %d: %s\n", yylineno, msg);
}
%}

%union {
    bool my_bool;
    uint64_t my_int;
    double my_double;
    char* my_str;
    ast_node* node;
    nodeType op_type;
}

%token L_PARENTHESIS
%token R_PARENTHESIS
%token L_BRACE
%token R_BRACE
%token L_BRACKET
%token R_BRACKET
%token COMMA
%token COLON
%token SELECT
%token INSERT
%token UPDATE
%token DELETE
%token VALUES
%token FILTER
%token<op_type> COMPARE_OP
%token<op_type> LIKE_OP
%token<op_type> LOGICAL_BOP
%token<op_type> LOGICAL_UOP
%token<my_bool> BOOL
%token<my_int> INT
%token<my_double> DOUBLE
%token<my_str> STRING
%token<my_str> NAME

%type<node> query
%type<node> select
%type<node> insert
%type<node> update
%type<node> delete
%type<node> select_next
%type<node> insert_or_select_next
%type<node> insert_next
%type<node> update_next
%type<node> select_object
%type<node> mutate_object
%type<node> values
%type<node> element
%type<node> key
%type<node> value
%type<node> filter
%type<node> operation
%type<node> compare_op
%type<node> like_op
%type<node> logical_op
%type<my_str> schema_name

%left COMMA

%%
init: query { print_node($1, 0); destroy_node($1); YYACCEPT; }

query: select { $$ = $1; } | insert { $$ = $1; } | update { $$ = $1; } | delete { $$ = $1; }

select: SELECT L_BRACE select_next R_BRACE { $$ = new_query_node(QUERY_SELECT_NODE, NULL, $3); }

insert: INSERT L_BRACE insert_or_select_next R_BRACE { $$ = new_query_node(QUERY_INSERT_NODE, NULL, $3); }

update: UPDATE L_BRACE update_next R_BRACE { $$ = new_query_node(QUERY_UPDATE_NODE, NULL, $3); }

delete: DELETE L_BRACE select_next R_BRACE { $$ = new_query_node(QUERY_DELETE_NODE, NULL, $3); }

select_next: select_object { $$ = new_query_set_node(new_query_node(NESTED_QUERY_NODE, $1, NULL)); } | select_object L_BRACE select_next R_BRACE { $$ = new_query_set_node(new_query_node(NESTED_QUERY_NODE, $1, $3)); } | select_next COMMA select_next { add_next_query_to_set($1, $3); $$ = $1; }

insert_or_select_next: insert_next %prec COMMA { $$ = $1; } | select_object L_BRACE insert_or_select_next R_BRACE { $$ = new_query_set_node(new_query_node(NESTED_QUERY_NODE, $1, $3)); } | insert_or_select_next COMMA insert_or_select_next { add_next_query_to_set($1, $3); $$ = $1; }

insert_next: mutate_object { $$ = new_query_set_node(new_query_node(NESTED_QUERY_NODE, $1, NULL)); } | mutate_object L_BRACE insert_next R_BRACE { $$ = new_query_set_node(new_query_node(NESTED_QUERY_NODE, $1, $3)); } | insert_next COMMA insert_next { add_next_query_to_set($1, $3); $$ = $1; }

update_next: mutate_object { $$ = new_query_set_node(new_query_node(NESTED_QUERY_NODE, $1, NULL)); } | mutate_object L_BRACE update_next R_BRACE { $$ = new_query_set_node(new_query_node(NESTED_QUERY_NODE, $1, $3)); } | select_object L_BRACE update_next R_BRACE { $$ = new_query_set_node(new_query_node(NESTED_QUERY_NODE, $1, $3)); } | update_next COMMA update_next { add_next_query_to_set($1, $3); $$ = $1; }

select_object: schema_name { $$ = new_object_node($1, NULL, NULL); } | schema_name L_PARENTHESIS filter R_PARENTHESIS { $$ = new_object_node($1, NULL, $3); }

mutate_object: schema_name L_PARENTHESIS values R_PARENTHESIS { $$ = new_object_node($1, $3, NULL); } | schema_name L_PARENTHESIS values COMMA filter R_PARENTHESIS { $$ = new_object_node($1, $3, $5); }

schema_name: NAME { if(strlen($1) > MAX_NAME_LENGTH) { yyerror("name of schema is too long"); YYABORT; } $$ = $1; }

values: VALUES COLON L_BRACKET element R_BRACKET { $$ = new_values_node($4); }

element: L_BRACE key COLON value R_BRACE { $$ = new_element_set_node(new_element_node($2, $4)); } | element COMMA element { add_next_element_to_set($1, $3); $$ = $1; }

key: NAME { if(strlen($1) > MAX_NAME_LENGTH) { yyerror("key is too long"); YYABORT; } $$ = new_key_node($1); }

value: BOOL { $$ = new_bool_node($1); } | INT { $$ = new_int_node($1); } | DOUBLE { $$ = new_double_node($1); } | STRING { $$ = new_str_node($1); }

filter: FILTER COLON operation { $$ = new_filter_node($3); }

operation: compare_op { $$ = $1; } | like_op { $$ = $1; } | logical_op { $$ = $1; }

compare_op: COMPARE_OP L_PARENTHESIS key COMMA value R_PARENTHESIS { $$ = new_operation_node($1, $3, $5); } | COMPARE_OP L_PARENTHESIS key COMMA key R_PARENTHESIS { $$ = new_operation_node($1, $3, $5); }

like_op: LIKE_OP L_PARENTHESIS key COMMA STRING R_PARENTHESIS { $$ = new_operation_node($1, $3, new_str_node($5)); } | LIKE_OP L_PARENTHESIS key COMMA key R_PARENTHESIS { $$ = new_operation_node($1, $3, $5); }

logical_op: LOGICAL_UOP L_PARENTHESIS operation R_PARENTHESIS { $$ = new_operation_node($1, $3, NULL); } | LOGICAL_BOP L_PARENTHESIS operation COMMA operation R_PARENTHESIS { $$ = new_operation_node($1, $3, $5); }

%%
