#ifndef _GRAPHQL_AST_H_
#define _GRAPHQL_AST_H_

#include <stdint.h>
#include <stdbool.h>

typedef enum nodeType {
    QUERY_SELECT_NODE,
    QUERY_INSERT_NODE,
    QUERY_UPDATE_NODE,
    QUERY_DELETE_NODE,
    QUERY_SET_NODE,
    NESTED_QUERY_NODE,
    OBJECT_NODE,
    VALUES_NODE,
    ELEMENT_SET_NODE,
    ELEMENT_NODE,
    KEY_NODE,
    INT_VAL_NODE,
    DOUBLE_VAL_NODE,
    BOOL_VAL_NODE,
    STR_VAL_NODE,
    FILTER_NODE,
    OP_EQ_NODE,
    OP_NEQ_NODE,
    OP_GT_NODE,
    OP_GTE_NODE,
    OP_LE_NODE,
    OP_LEE_NODE,
    OP_LIKE_NODE,
    OP_AND_NODE,
    OP_OR_NODE,
    OP_NOT_NODE,
} nodeType;

typedef struct ast_node {
    struct ast_node* left;
    struct ast_node* right;
    nodeType type;
    union {
        uint64_t intVal;
        double doubleVal;
        bool boolVal;
        char* strVal;
    };
} ast_node;

ast_node* new_node();

ast_node* new_int_node(int32_t);

ast_node* new_double_node(double);

ast_node* new_bool_node(bool);

ast_node* new_str_node(char*);

ast_node* new_key_node(char* key);

ast_node* new_element_node(ast_node*, ast_node*);

ast_node* new_element_set_node(ast_node*);

ast_node* new_values_node(ast_node*);

ast_node* new_operation_node(nodeType, ast_node*, ast_node*);

ast_node* new_filter_node(ast_node*);

ast_node* new_object_node(char*, ast_node*, ast_node*);

ast_node* new_query_set_node(ast_node*);

ast_node* new_query_node(nodeType, ast_node*, ast_node*);

void add_next_element_to_set(ast_node*, ast_node*);

void add_next_query_to_set(ast_node*, ast_node*);

void destroy_node(ast_node*);

void print_node(ast_node*, int32_t);

#endif