# Low Level Programming Lab1 Drobysh Dmitry P33082 (ISU:333219)

### How to run this code?😄
To run this lab, you need to fork this repository:

main.c file reads file "data", you may change it if you want.
```
$ make
```
(it should be "main" file)

To run this app type this in your terminal:
```
$ ./main
```

### Are there any opportunities to test this code?🥹
I've added the benchmark here))

It works with hyperfine

in file called "input" you can add requests (rules you can find in https://gitlab.se.ifmo.ru/NorthCapDiamond/low_level_programming_lab1/-/blob/master/format.txt)

and run this code: 
```
$ ./bench.zsh
```
(Change zsh to your *sh)


Also Now there is a test.c, that generates add.csv and delete.csv files:

```
$ make
$ ./test
```

To visualize this data type:
 
```
$ python3 tester.py
```
or 
```
$ python tester.py
```

